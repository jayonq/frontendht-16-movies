import { createStore } from "vuex";
import api from "../api/tvmaze.js";

export default createStore({
  state: {
    movies: [],
    isLoading: false,
  },
  getters: {
    movies: (state) => state.movies,
    isLoading: (state) => state.isLoading,
    greaterThan9Films: (state) => {
      return state.movies.filter((movie) => movie.show.imDbRating >= 7);
    },
    after2000Films: (state) =>
      state.movies.filter((movie) => movie.show.year >= 2000),
  },
  mutations: {
    setMovies: (state, payload) => {
      payload.map((item) => {
        item.show.imDbRating = generateRandomNumber();
        item.show.year = generateRandomYear();
        return item;
      });

      state.movies = payload;
    },
    isLoading(state, payload) {
      state.isLoading = payload;
    },
  },
  actions: {
    async fetchMovies({ commit }) {
      commit("isLoading", true);
      const response = await api.fetchMovies();
      commit("setMovies", response.data);
      commit("isLoading", false);
    },
  },
  modules: {},
});

function generateRandomNumber() {
  return Math.floor(Math.random() * 10) + 1;
}

function generateRandomYear() {
  const random = Math.floor(Math.random() * 11);
  const year = 1995 + random;
  return year;
}
