import axios from "axios";

const ROOT_URL = "https://api.tvmaze.com";

export default {
  fetchMovies() {
    return axios.get(`${ROOT_URL}/search/shows/?q=film`);
  },

  searchMovies(text) {
    if (text !== null) {
      return axios.get(`${ROOT_URL}/search/shows/?q=${text}`);
    }
  },
};
